# dualtitles

Align and create dual subtitles in your browser.

## Usage

Go to [simonrepp.com/dualtitles](https://simonrepp.com/dualtitles/) or alternatively clone/download this repository and open `index.html` in your browser.

- Use the top menu to import and export subtitles
- Use the small bar at the bottom to navigate the timeline (Click and drag)
- Align subtitles by finding and consecutively clicking pairs of matching subtitles. (E.g.: To synchronize spanish subtitles for "The empire strikes back" against english subtitles for that same movie, you could first look for the line "No, I am your father" in the english subtitles, click it, then look for "No, soy tu padre!" in the spanish subtitles, and click that too; dualtitles will then automatically shift these and all other lines to fit against each other; Sometimes one pair of matching lines is enough to synchronize your subtitles, sometimes you will need at least two matching lines for a correct alignment)
- Check the advanced options for important import settings, output formats and for playing around with subtitle styles for the .ass exporter

## Hints

- If a file import fails, don't panic! Converting your subtitles to UTF-8 or specifying the right encoding under the advanced options is your first big chance for success. If it still fails to import, you probably caught a malformed SRT (Not too uncommon unfortunately). Fire up the console to catch the JISON parser output with a reference to the faulty line number. Investigate in your favorite text editor. usually you're dealing with an empty subtitle or invalid blank lines inside a subtitle text.
- Aligning subtitles by picking only one or (if necessary) two pairs works best - picking more pairs is usually counterproductive; But choosing the right pairs for those two matches is crucial for success! (Stick to the visual patterns to find good potential matching points)

## License

dualtitles is licensed under the GPLv3
