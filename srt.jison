
/* SRT Parser. */

/* lexical grammar */
%lex
%%

\r\n|\r|\n            return 'EOL'
" --> "               return 'ARROW'
":"                   return ':'
'.'                   return '.'
","                   return ','
[0-9]+                return 'NUMBER'
[^\r\n]+              return 'STRING'
<<EOF>>               return 'EOF'
^                     return 'EOF'

/lex

%start srt

%% /* language grammar */

srt
    : subtitles
        { return $subtitles; }}
    ;

subtitles
    : subtitles subtitle
        {$subtitles.push($subtitle); $$ = $subtitles;}
    | subtitle
        {$$ = [$subtitle];}
    ;

subtitle
    : NUMBER EOL timecode ARROW timecode EOL lines eos
        {$$ = { id: parseInt($NUMBER), start: $timecode1, end: $timecode2, lines: $lines };}
    ;

timecode
    : NUMBER ':' NUMBER ':' NUMBER delimiter NUMBER
        {$$ = parseInt($NUMBER1 * 3600) + parseInt($NUMBER2 * 60) + parseInt($NUMBER3) + parseFloat($NUMBER4 / 1000);}
    ;

delimiter
    : ','
    | '.'
    ;

lines
    : line lines 
        {$$ = $line + $lines;}    
    | line
        {$$ = $line;}
    ;

line
    : text line
        {$$ = $text + $line;}
    | text lineterm
        {$$ = $text;}
    ;

text
    : '.'
    | ','
    | ':'
    | ARROW
    | NUMBER    
    | STRING
    ;    

lineterm
    : EOL
    | EOF
    ;

eos
    : EOL eos
    | EOL
    | EOF
    ;