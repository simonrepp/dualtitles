const downloadAnchor = document.querySelector('#download');
const downloadDual = document.querySelector('#download_dual');
const downloadSingle0 = document.querySelector('#download_single_0');
const downloadSingle1 = document.querySelector('#download_single_1');
const fontInput = document.querySelector('#font');
const openTrack0Button = document.querySelector('#open_track_0_button');
const openTrack0Input = document.querySelector('#open_track_0_input');
const openTrack1Button = document.querySelector('#open_track_1_button');
const openTrack1Input = document.querySelector('#open_track_1_input');
const primaryColorInput = document.querySelector('#primary-font-color');
const primarySizeInput = document.querySelector('#primary-font-size');
const secondaryColorInput = document.querySelector('#secondary-font-color');
const secondarySizeInput = document.querySelector('#secondary-font-size');
const settings = document.querySelector('#settings');
const settingsButton = document.querySelector('#toggle_settings');
const undoButton = document.querySelector('#undo');

const assTimecodeFormat = d3.time.format('%-H:%M:%S.%L');
const srtTimecodeFormat = d3.time.format('%H:%M:%S,%L');

function exportSRT(subtitles) {
    let result = '';

    subtitles.forEach(subtitle => {
        const { end, id, start } = subtitle;
        const startDate = new Date(666, 0, 1, 0, 0, Math.floor(start), (start - Math.floor(start)) * 1000);
        const endDate = new Date(666, 0, 1, 0, 0, Math.floor(end), (end - Math.floor(end)) * 1000);

        result += id + '\n';
        result += `${srtTimecodeFormat(startDate)} --> ${srtTimecodeFormat(endDate)}\n`;
        result += subtitle.lines + '\n\n';
    });

    return result;
}

function exportASS(primarySubtitles, secondarySubtitles) {
    const font = fontInput.value;
    const primaryHex = primaryColorInput.value;
    const primarySize = primarySizeInput.value;
    const secondaryHex = secondaryColorInput.value;
    const secondarySize = secondarySizeInput.value;

    const primaryColor = '&H00' + primaryHex.slice(5,7) + primaryHex.slice(3,5) + primaryHex.slice(1,3);
    const secondaryColor = '&H00' + secondaryHex.slice(5,7) + secondaryHex.slice(3,5) + secondaryHex.slice(1,3);

    let result = '';
    result += '[Script Info]\nScriptType: v4.00+\n\n';
    result += '[V4+ Styles]\nFormat: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding\n';
    result += `Style: Default,${font},${primarySize},${primaryColor},${primaryColor},&H40000000,&H40000000,0,0,0,0,100,100,0,0.00,1,3,0,2,20,20,20,1\n`;
    result += `Style: Secondary,${font},${secondarySize},${secondaryColor},${secondaryColor},&H40000000,&H40000000,0,0,0,0,100,100,0,0.00,1,3,0,2,20,20,10,1\n`;
    result += '\n'; // Close styles section
    result += '[Events]\nFormat: Layer, Start, End, Style, Actor, MarginL, MarginR, MarginV, Effect, Text\n';

    const appendSubtitle = style => {
        return subtitle => {
            const { end, start } = subtitle;

            const sd = new Date(666, 0, 1, 0, 0, Math.floor(start), (start - Math.floor(start)) * 1000);
            const ed = new Date(666, 0, 1, 0, 0, Math.floor(end), (end - Math.floor(end)) * 1000);

            fields = [
                'Dialogue: 0',                        // Format: Marked
                assTimecodeFormat(sd),                // Start
                assTimecodeFormat(ed),                // End
                style,                                // Style
                '',                                   // Name
                '0000',                               // MarginL
                '0000',                               // MarginR
                '0000',                               // MarginV
                '',                                   // Effect
                subtitle.lines.replace(/\n/g, '\N')   // Text
            ];

            result += fields.join(',') + '\n';
        };
    };

    primarySubtitles.forEach(appendSubtitle('Default'));
    secondarySubtitles.forEach(appendSubtitle('Secondary'));

    return result;
}

// Coordinates, Dimensions, Paddings

const VIEW_HEIGHT = 400;
const VIEW_TRACK_HEIGHT = 80;
const VIEW_TRACK_Y = [10, 310];
const VIEW_TRACK_LABEL_Y = [130, 280];
const OVERVIEW_Y = 420;
const OVERVIEW_HEIGHT = 40;

const BASE_COLOR = d3.hsl(22, 0.8, 0.6);
const RAMP_COLOR = d3.hsl(62, 1, 0.8);
const MATCH_COLOR = d3.hsl(120, 0.25, 0.5);
const MISMATCH_COLOR = d3.hsl(0, 0.25, 0.5);
const TIE_COLOR = d3.hsl(42, 1, 0.5);

const TRANSITIONS_DURATION = 400;

const DEFAULT_SUBTITLES_0 = [
    { id: 1, start: 0, end: 1, lines: `Welcome to dualtitles. Try moving your mouse over the other subtitle blocks to learn all about the interface!` },
    { id: 2, start: 2, end: 3, lines: `Navigate the timeline by clicking and dragging on the small bar at the bottom.` },
    { id: 3, start: 4, end: 5, lines: `Use the "Change" buttons to load new subtitles into the tracks.` },
    { id: 4, start: 6, end: 7, lines: `To begin aligning subtitles, click on a subtitle block on the track where the timecodes are off.` },
    { id: 5, start: 8, end: 9, lines: `To align it, search the matching subtitle block in the other track and click on it too.` },
    { id: 6, start: 10, end: 11, lines: `As you will see, dualtitles will have matched that exact pair, but depending on your subtitles, the rest may still be off.` },
    { id: 7, start: 12, end: 13, lines: `In that case, repeat the last step for another pair of subtitles.` },
    { id: 8, start: 14, end: 15, lines: `If you chose two good pairs, you'll be done by now. :)` },
    { id: 9, start: 16, end: 17, lines: `If your subtitles still don't match, you should hit "Undo" (once or twice) and try other pairs, until you achieve a perfect match.` },
    { id: 10, start: 18, end: 19, lines: `Another way to improve the alignment is to go on selecting more and more pairs, but this approach produces inferior results and I definitely don't recommend it.` },
    { id: 11, start: 20, end: 21, lines: `When you're done with the alignment process, use one of the "Export" buttons for exporting either a single track, or both tracks combined in a single file.` },
    { id: 12, start: 22, end: 23, lines: `Under "Settings" you can specify the import encoding (Your first bet when importing fails), as well as format and style options for the exporter.` },
    { id: 13, start: 24, end: 25, lines: `That's it! If you find any issues please file a report so we can make the tool better for everyone.` }
];

const DEFAULT_SUBTITLES_1 = [
    { id: 1, start: 0, end: 2, lines: `Willkommen bei dualtitles. Bewege den Maus-Cursor über die anderen Untertitelblöcke um alles über das Interface zu lernen.` },
    { id: 2, start: 4, end: 6, lines: `Navigiere die Timeline in der schmalen, unteren Leiste via Click and Drag.` },
    { id: 3, start: 8, end: 10, lines: `Verwende die "Change" Buttons um neue Untertitel in die einzelnen Spuren zu laden.` },
    { id: 4, start: 12, end: 14, lines: `Um die Untertitelspuren einander anzugleichen, klicke zuerst auf einen Untertitelblock in der Spur, in der die Timecodes nicht stimmen.` },
    { id: 5, start: 16, end: 18, lines: `Dann suche den dazugehörigen Untertitelblock in der anderen Spur und klicke auch auf diesen.` },
    { id: 6, start: 20, end: 22, lines: `Wie du sehen wirst, wird dualtitles dieses eine Paar genau angeglichen haben, abhängig von deinen Untertiteln könnten die anderen Blöcke aber nach wie vor verschoben sein.` },
    { id: 7, start: 24, end: 26, lines: `In diesem Fall, wiederhole den letzten Schritt für ein weiteres Untertitel Paar.` },
    { id: 8, start: 28, end: 30, lines: `Wenn du zwei gute Paare erwischt hast, wirst du nun fertig sein. :)` },
    { id: 9, start: 32, end: 34, lines: `Wenn sie immer noch nicht übereinstimmen, solltest du "Undo" drücken (ein oder zweimal) und weitere Paare probieren, bis du eine perfekte Übereinstimmung erzielst.` },
    { id: 10, start: 36, end: 38, lines: `Ein weiterer Weg die Übereinstimmung zu verbessern, ist es, noch mehr Paare zu suchen, da diese Herangehensweise aber schlechtere Resultate produziert, rate ich davon ab.` },
    { id: 11, start: 40, end: 42, lines: `Wenn du mit dem Angleichen fertig bist, verwende einen der "Export" Buttons um entweder eine einzelne, oder beide Spuren kombiniert in einem File zu exportieren.` },
    { id: 12, start: 44, end: 46, lines: `Unter "Settings" kannst du das Import encoding (Der erste Ansatz falls das Importieren schiefgeht), sowie Format und Style Optionen für den Exporter festlegen.` },
    { id: 13, start: 48, end: 50, lines: `Das war's! Wenn du irgendwelche Probleme findest, reporte sie bitte damit wir das Tool für alle verbessern können.` }
];

const INITIAL_VIEW = [
    DEFAULT_SUBTITLES_0[0].start,
    DEFAULT_SUBTITLES_0[DEFAULT_SUBTITLES_0.length - 1].end
];

const INITIAL_OVERVIEW = [
    DEFAULT_SUBTITLES_0[0].start,
    DEFAULT_SUBTITLES_1[DEFAULT_SUBTITLES_1.length - 1].end
];

// Subtitle tracks

const tracks = [
  { subtitles: DEFAULT_SUBTITLES_0 },
  { subtitles: DEFAULT_SUBTITLES_1 }
];

tracks[0].subtitles.forEach(subtitle => { subtitle.track = 0; });
tracks[1].subtitles.forEach(subtitle => { subtitle.track = 1; });

let width = window.innerWidth;
let height = window.innerHeight;

const svg = d3.select('#container').append('svg:svg');

const subtitleColorScale = d3.scale
    .linear()
    .range([BASE_COLOR,RAMP_COLOR])
    .interpolate(d3.interpolateHsl)
    .clamp(true);

const viewScale = d3.scale
    .linear()
    .domain(INITIAL_VIEW)
    .range([20, width - 20]);

const overviewScale = d3.scale
    .linear()
    .domain(INITIAL_OVERVIEW)
    .range([20, width - 20]);

// Track storage

let allSubtitles;
let timecodeExtent;
let smoothedLengthExtent;

// Initialise match statistics

let matchGraph = [];
let matchPercentage = 0;

const brush = d3.svg.brush()
    .x(overviewScale)
    .extent(INITIAL_VIEW)
    .on('brushstart', () => { svg.select('#brush-timespan').attr('opacity', 1); })
    .on('brush', () => updateOverviewTimespan())
    .on('brushend', () => {
        svg.select('#brush-timespan').attr('opacity', 0);
        zoom();
    });

// Each tie represents an association of a subtitle in the upper track with a
// subtitle in the lower track, as manually created by the user.
let ties = [];

let cursorX;
let cursorY;

const viewAxis = d3.svg
    .axis()
    .scale(viewScale)
    .orient('bottom')
    .ticks(4)
    .tickFormat(timecodeFormat);

// Initialise view

svg.append('g').attr('id', 'view');

svg.select('#view').append('rect').attr('id', 'view-frame');
svg.select('#view').append('g').attr('id', 'view-matchgraph');
svg.select('#view').append('g').attr('id', 'view-subtitles');
svg.select('#view').append('line').attr('id', 'view-separator');
svg.select('#view').append('g').attr('id', 'view-axis');
svg.select('#view').append('g').attr('id', 'view-lines');
svg.select('#view').append('g').attr('id', 'view-drag');
svg.select('#view').append('text').attr('id', 'center-text');
svg.select('#view').append('text').attr('id', 'center-subtext');
svg.select('#view').append('line').attr('id', 'cursor');

svg.select('#view-separator')
    .attr('x1', 0)
    .attr('y1', VIEW_HEIGHT / 2)
    .attr('x2', width)
    .attr('y2', VIEW_HEIGHT / 2)
    .attr('stroke', '#888')
    .attr('stroke-width', 0.25);

svg.select('#view-axis')
    .attr('transform', `translate(0, ${VIEW_HEIGHT / 2})`);

svg.select('#center-text')
    .attr('x', width / 2)
    .attr('y', VIEW_HEIGHT / 2 + 5)
    .attr('text-anchor', 'middle');

svg.select('#center-subtext')
    .attr('x', width / 2)
    .attr('y', VIEW_HEIGHT / 2 + 35)
    .attr('text-anchor', 'middle');

svg.select('#cursor')
    .attr('stroke', '#888')
    .attr('stroke-width', 0.25)
    .attr('pointer-events', 'none');

// Initialise overview
svg.append('g').attr('id', 'overview')

svg.select('#overview').append('rect').attr('id', 'overview-frame');
svg.select('#overview').append('g').attr('id', 'overview-subtitles');
svg.select('#overview').append('line').attr('id', 'overview-separator');
svg.select('#overview').append('g').attr('id', 'overview-ties');
svg.select('#overview').append('text').attr('id', 'brush-timespan');

svg.select('#overview')
   .attr('transform', `translate(0, ${OVERVIEW_Y})`);

svg.select('#overview-separator')
    .attr('x1', overviewScale.range()[0])
    .attr('y1', OVERVIEW_HEIGHT / 2)
    .attr('x2', overviewScale.range()[1])
    .attr('y2', OVERVIEW_HEIGHT / 2)
    .attr('stroke', '#888')
    .attr('stroke-width', 0.25);

svg.select('#overview').append('g')
    .attr('id', 'brush')
    .attr('class', 'x brush')
    .attr('y', OVERVIEW_HEIGHT + VIEW_HEIGHT)
    .call(brush)
    .selectAll('rect')
    .attr('y', 0)
    .attr('height', OVERVIEW_HEIGHT);

svg.select('#brush-timespan')
    .attr('text-anchor', 'middle');

updateSubtitles();
resize();

function downloadFile(filename, content) {
    downloadAnchor.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(content));
    downloadAnchor.setAttribute('download', filename);
    downloadAnchor.click();
}

undoButton.addEventListener('click', () => {
    const tie = ties.pop();

    for (const subtitle of tie) { subtitle.tied = false; }

    for (const subtitle of allSubtitles) {
        const { end, start } = subtitle.states.pop();
        subtitle.start = start;
        subtitle.end = end;
    }

    if (ties.length === 0) {
        undoButton.setAttribute('disabled', 'disabled');
    }

    updateSubtitles();
    updateGraphs();
});

settingsButton.addEventListener('click', () => {
    settings.style.display = (settings.style.display === 'none' ? 'block' : 'none');
});

d3.select('#view').on('mousemove', updateCursor);

function importSubtitles(input, track) {
    const encoding = document.querySelector('#open-with-encoding').value;
    const file = input.files[0];
    const reader = new FileReader();

    reader.onload = () => {
        ties = [];

        tracks[track === 0 ? 1 : 0].subtitles.forEach(subtitle => {
            subtitle.states = [];
        });

        tracks[track].filename = input.value;
        tracks[track].subtitles = srt.parse(reader.result + '\n');
        tracks[track].subtitles.forEach(subtitle => { subtitle.track = track; });

        updateSubtitles();
    }

    document.querySelector(`#filename_${track}`).innerHTML = file.name;

    reader.readAsText(file, encoding);
}

function downloadSubtitles(trackIndex) {
    const format = document.querySelector('#single_format').value;

    if (format === '.srt') {
        downloadFile(`single-${trackIndex}.srt`, exportSRT(tracks[trackIndex].subtitles));
    } else {
        downloadFile(`single-${trackIndex}.ass`, exportASS(tracks[trackIndex].subtitles, []));
    }
}

openTrack0Button.addEventListener('click', () => openTrack0Input.click());
openTrack0Input.addEventListener('change', () => importSubtitles(openTrack0Input, 0));
downloadSingle0.addEventListener('click', () => downloadSubtitles(0));

openTrack1Button.addEventListener('click', () => openTrack1Input.click());
openTrack1Input.addEventListener('change', () => importSubtitles(openTrack1Input, 1));
downloadSingle1.addEventListener('click', () => downloadSubtitles(1));

downloadDual.addEventListener('click', () => {
    const emphasizedTrack = document.querySelector('#dual_emphasis').value;
    const [primary, secondary] = emphasizedTrack === '0' ? [0, 1] : [1, 0]

    downloadFile('dual.ass', exportASS(tracks[primary].subtitles, tracks[secondary].subtitles));
});

window.onresize = resize;

function resize() {
    width = window.innerWidth;
    height = window.innerHeight;

    // Resize svg
    svg
      .attr('width', width)
      .attr('height', OVERVIEW_Y + OVERVIEW_HEIGHT);

    // Resize scales
    overviewScale.range([20, width - 20])
    viewScale.range([20, width - 20]);

    brush.extent(viewScale.domain());
    svg.select('#brush').call(brush);

    svg.select('#view-axis').call(viewAxis);

    // Resize backgrounds
    svg.select('#view-frame')
      .attr('x', 0)
      .attr('y', 0)
      .attr('width', width)
      .attr('height', VIEW_HEIGHT)
      .attr('fill', '#eee')
      .attr('stroke', '#888')
      .attr('stroke-width', '0.25');

    svg.select('#view-separator')
      .attr('x1', 0)
      .attr('x2', width);

    svg.select('#center-text')
      .attr('x', (viewScale.range()[0] + viewScale.range()[1]) / 2);

    svg.select('#center-subtext')
      .attr('x', (viewScale.range()[0] + viewScale.range()[1]) / 2);

    svg.select('#overview-frame')
      .attr('x', overviewScale.range()[0])
      .attr('y', 0)
      .attr('width', overviewScale.range()[1] - overviewScale.range()[0])
      .attr('height', OVERVIEW_HEIGHT)
      .attr('fill', '#eee')
      .attr('stroke', '#888')
      .attr('stroke-width', '0.25');

    svg.select('#overview-separator')
      .attr('x1', overviewScale.range()[0])
      .attr('x2', overviewScale.range()[1])

    updateGraphs();
}

function updateSubtitles() {
    allSubtitles = [...tracks[0].subtitles, ...tracks[1].subtitles];

    calculateCoverage();
    calculateExtents();
    calculateMatch();

    subtitleColorScale.domain(smoothedLengthExtent);
    overviewScale.domain(timecodeExtent);

    svg.select('#brush').call(brush);

    if (viewScale.domain()[0] < timecodeExtent[0]) {
        viewScale.domain([timecodeExtent[0], viewScale.domain()[1]]);
    } else if (viewScale.domain()[0] > timecodeExtent[1]) {
        viewScale.domain([timecodeExtent[0], timecodeExtent[1]]);
    }

    if (viewScale.domain()[1] > timecodeExtent[1]) {
        viewScale.domain([viewScale.domain()[0], timecodeExtent[1]]);
    } else if (viewScale.domain()[1] < timecodeExtent[0]) {
        viewScale.domain([timecodeExtent[0], timecodeExtent[1]]);
    }

    updateGraphs();
}

function calculateCoverage() {
    const sum = (r, d) => r + (d.end - d.start);

    tracks[0].coverage = tracks[0].subtitles.reduce(sum, 0);
    tracks[1].coverage = tracks[1].subtitles.reduce(sum, 0);
}

function calculateExtents() {
    timecodeExtent = d3.extent([
        tracks[0].subtitles[0].start,
        tracks[1].subtitles[0].start,
        tracks[0].subtitles[tracks[0].subtitles.length - 1].end,
        tracks[1].subtitles[tracks[1].subtitles.length - 1].end
    ]);

    const lengths = allSubtitles.map(d => d.end - d.start).sort(d3.ascending);

    smoothedLengthExtent = [d3.quantile(lengths, 0.25), d3.quantile(lengths, 0.75)];
}

function zoom() {
    if (brush.empty()) {
        viewScale.domain(overviewScale.domain());
    } else {
        if (overviewScale(brush.extent()[0]) <= overviewScale.range()[0] + 2 &&
           overviewScale(brush.extent()[1]) >= overviewScale.range()[1] - 2) {
            viewScale.domain(overviewScale.domain());
            brush.clear();
            svg.select('#brush').call(brush);
        } else {
           viewScale.domain(brush.extent());
        }
    }

    svg.select('#view-axis').call(viewAxis);

    updateGraphs();
}

function timespanFormat(d) {
    const date = new Date(666, 0, 1, 0, 0, Math.floor(d), (d - Math.floor(d)) * 1000);

    if (d < 1) {
        return d3.time.format('%-L milliseconds')(date);
    } else if (d < 60) {
        return d3.time.format('%-S seconds')(date);
    } else if (d < 3600) {
        return d3.time.format('%-M minutes')(date);
    } else {
        return d3.time.format('%-H hours %-M minutes')(date);
    }
}

function timecodeFormat(d) {
    const date = new Date(666, 0, 1, 0, 0, Math.floor(d), (d - Math.floor(d)) * 1000);
    return d3.time.format('%H:%M:%S,%L')(date);
}

function calculateMatch() {
    let matching = 0;
    let mismatching = 0;

    const cuts = []

    allSubtitles.forEach(d => {
        if (cuts.indexOf(d.start) === -1) { cuts.push(d.start); }
        if (cuts.indexOf(d.end) === -1) { cuts.push(d.end); }
    });

    const frames = d3.pairs(cuts.sort(d3.ascending));

    matchGraph = [];

    frames.forEach(frame => {
        const duration = frame[1] - frame[0];

        const intersection = ({ end, start }) => 
            start === frame[0] || end === frame[1] ||
            (start < frame[0] && end > frame[1]);

        const intersections = [
            tracks[0].subtitles.filter(intersection),
            tracks[1].subtitles.filter(intersection)
        ];

        if (intersections[0].length > 0 && intersections[1].length > 0) {
            matchGraph.push({ start: frame[0], end: frame[1], match: [true, true] });
            matching += duration;
        } else if (intersections[0].length === 0 && intersections[1].length > 0) {
            matchGraph.push({ start: frame[0], end: frame[1], match: [false, true] });
            mismatching += duration;
        } else if (intersections[0].length > 0 && intersections[1].length === 0) {
            matchGraph.push({ start: frame[0], end: frame[1], match: [true, false] });
            mismatching += duration;
        }
    });

    matchPercentage = Math.floor((matching / (matching + mismatching)) * 100);
}

function updateMatchgraph() {
    centerText(matchPercentage + '%', 'Degree of correlation');

    const matchFrames = svg.select('#view-matchgraph').selectAll('rect').data(matchGraph);

    matchFrames.enter().append('rect');

    matchFrames
        .transition()
        .duration(TRANSITIONS_DURATION)
        .attr('x', d => viewScale(d.start))
        .attr('y', 0)
        .attr('width', d => viewScale(d.end) - viewScale(d.start))
        .attr('height', VIEW_HEIGHT)
        .attr('fill', d => (d.match[0] && d.match[1]) ? MATCH_COLOR : MISMATCH_COLOR)
        .attr('fill-opacity', d => (d.match[0] && d.match[1]) ? 0.2 : 0.1);

    matchFrames.exit().remove();
}

function updateView() {
    const trackGroups = svg.select('#view-subtitles').selectAll('.track').data(tracks);

    trackGroups.enter()
        .append('g')
        .attr('class', 'track')
        .attr('transform', (d, i) => `translate(0, ${VIEW_TRACK_Y[i]})`);

    const subtitles = trackGroups.selectAll('rect').data(d => d.subtitles, d => d.id);

    subtitles.enter().append('rect').attr('class', 'subtitle');

    subtitles
        .on('click', d => tie(d))
        .transition()
        .duration(TRANSITIONS_DURATION)
        .attr('x', d => viewScale(d.start))
        .attr('y', 0)
        .attr('width', d => viewScale(d.end) - viewScale(d.start))
        .attr('height', VIEW_TRACK_HEIGHT)
        .attr('fill', d => d.tied ? TIE_COLOR : subtitleColorScale(d.end - d.start));

    subtitles.exit().remove();
}

function updateOverview() {
    const trackGroups = svg.select('#overview-subtitles').selectAll('.track').data(tracks);

    trackGroups.enter()
        .append('g')
        .classed('track', true)
        .attr('transform', (d, i) => `translate(0, ${i * (OVERVIEW_HEIGHT / 2)})`);

    const subtitles = trackGroups.selectAll('rect').data(d => d.subtitles, d => d.id);

    subtitles.enter().append('rect');

    subtitles
        .transition()
        .duration(TRANSITIONS_DURATION)
        .attr('x', d => overviewScale(d.start))
        .attr('y', 0)
        .attr('width', d => overviewScale(d.end) - overviewScale(d.start))
        .attr('height', OVERVIEW_HEIGHT / 2)
        .attr('fill', d => d.tied ? TIE_COLOR : subtitleColorScale(d.end - d.start));

    subtitles.exit().remove();
}

function updateOverviewTimespan() {
    if (brush.empty()) {
        svg.select('#brush-timespan').attr('opacity', '0')
    } else {
        svg.select('#brush-timespan')
            .attr('x', overviewScale((brush.extent()[0] + brush.extent()[1]) / 2))
            .attr('y', OVERVIEW_HEIGHT / 2 + 5)
            .text(timespanFormat(brush.extent()[1] - brush.extent()[0]))
            .attr('opacity', '1');
    }
}

function updateGraphs() {
    updateMatchgraph();
    updateView();
    updateOverview();
    updateTies();
    updateDrag();
    updateHoverLines();
}

function updateTies() {
    const s = OVERVIEW_HEIGHT / 4;

    const tiePaths = svg.select('#overview-ties')
        .selectAll('path')
        .data(ties.reduce((a, b) => a.concat(b), []));

    tiePaths.enter().append('path');

    tiePaths
        .attr('stroke', TIE_COLOR)
        .attr('stroke-width', 1)
        .attr('fill', 'none')
        .attr('d', d => {
            const x = overviewScale(d.start);

            if (d.track === 0) {
                return `M ${x - s / 2} 0 h ${s} l ${(s / 2) * -1} ${s} z`;
            } else {
                return `M ${x - s / 2} ${OVERVIEW_HEIGHT} h ${s} l ${(s / 2) * -1} ${s * -1} z`;
            }
        });

    tiePaths.exit().remove();
}

function updateCursor() {
    cursorX = d3.mouse(this)[0];
    cursorY = d3.mouse(this)[1];

    svg.select('#cursor')
        .attr('x1', cursorX)
        .attr('x2', cursorX)
        .attr('y1', 0)
        .attr('y2', VIEW_HEIGHT);

    updateDrag();
    updateHoverLines();
}

function updateDrag() {
    svg.selectAll('#view-drag path').remove();

    if (ties.length > 0 && ties[ties.length - 1].length === 1) {
        const originTrack = ties[ties.length - 1][0].track;
        const targetTrack = (originTrack === 0 ? 1 : 0);

        const barLength = viewScale(ties[ties.length - 1][0].end) -
                        viewScale(ties[ties.length - 1][0].start);

        let snap = cursorX;

        if (cursorY >= VIEW_TRACK_Y[targetTrack] &&
            cursorY <= VIEW_TRACK_Y[targetTrack] + VIEW_TRACK_HEIGHT) {

            const snappable = tracks[targetTrack].subtitles.filter(subtitle => {
                return subtitle.start <= viewScale.invert(cursorX) &&
                     subtitle.end >= viewScale.invert(cursorX);
            });

            if (snappable.length === 1) {
                snap = viewScale(snappable[0].start);
            }
        }

        // Render target frame

        svg.select('#view-drag').append('path')
            .attr('stroke', '#444')
            .attr('stroke-width', 1)
            .attr('fill', 'none')
            .attr('d', () =>
                `M ${snap} ${VIEW_TRACK_Y[originTrack]} h ${barLength} v ${VIEW_TRACK_HEIGHT} h ${-barLength} Z`
            );

        // Render target arrows if there is space to fill

        function renderDragArrows(x1, x2) {
            const distance = x2 - x1;

            svg.select('#view-drag').append('path')
                .attr('fill', '#888')
                .attr('fill-opacity', 0.2)
                .attr('d', () =>
                    `M ${x1} ${VIEW_TRACK_Y[originTrack]} h ${distance} v ${VIEW_TRACK_HEIGHT} h ${-distance} Z`
                );

            const pad = distance < 0 ? -16 : 16;
            const thickness = distance < 0 ? -32 : 32;
            const arrows = Math.floor(Math.abs((distance - pad) / (thickness * 2)));

            for (let i = 0; i < arrows; i++) {
                const corner = x2 - (pad / 2) - (thickness * 2 * i);

                svg.select('#view-drag').append('path')
                    .attr('fill', '#888')
                    .attr('d', () => 
                        `M ${corner} ${VIEW_TRACK_Y[originTrack] + VIEW_TRACK_HEIGHT / 2} ` +
                        `l ${-thickness} ${VIEW_TRACK_HEIGHT / 2} ` +
                        `h ${-thickness} ` +
                        `l ${thickness} ${VIEW_TRACK_HEIGHT / -2} ` +
                        `l ${-thickness} ${VIEW_TRACK_HEIGHT / -2} ` +
                        `h ${thickness} ` +
                        'Z'
                    );
            }
        }

        if (snap + barLength < viewScale(ties[ties.length - 1][0].start)) {
            renderDragArrows(viewScale(ties[ties.length - 1][0].start), snap + barLength);
        } else if (snap > viewScale(ties[ties.length - 1][0].end)) {
            renderDragArrows(viewScale(ties[ties.length - 1][0].end), snap);
        }
    }
}

function updateHoverLines() {
    if (cursorX) {
        const cursorHovered = allSubtitles.filter(subtitle => 
            subtitle.start <= viewScale.invert(cursorX) &&
            subtitle.end >= viewScale.invert(cursorX)
        );

        const lines = svg.select('#view-lines').selectAll('text').data(cursorHovered);

        lines.enter().append('text');

        lines
            .attr('x', cursorX + 10)
            .attr('y', d => VIEW_TRACK_LABEL_Y[d.track])
            .text(d => d.lines)
            .each(function(d) {
                if (cursorX + 20 + this.getComputedTextLength() > viewScale.range()[1]) {
                    this.setAttribute('x', viewScale.range()[1] - this.getComputedTextLength() - 10);
                }
            });

        lines.exit().remove();
    }
}

function centerText(text, subtext) {
    svg.select('#center-text').text(text);
    svg.select('#center-subtext').text(subtext);
}

function tie(subtitle) {
    subtitle.tied = true;

    if (ties.length === 0 || ties[ties.length - 1].length === 2) {
        ties.push([subtitle]);
        updateGraphs();
    } else {
        if (ties[ties.length - 1][0].track === subtitle.track) {
            ties[ties.length - 1][0].tied = false;
            ties[ties.length - 1][0] = subtitle;
            updateGraphs();
        } else {
            ties[ties.length - 1].push(subtitle);

            centerText('Tying ...', 'Might take a few seconds');
            setTimeout(commitTies, 50);
        }
    }
}

function commitTies() {
    allSubtitles.forEach(subtitle => {
        const { end, start } = subtitle;

        if (subtitle.states === undefined) {
            subtitle.states = [{ end, start }];
        } else {
            subtitle.states.push({ end, start });
        }
    });

    if (ties.length > 1) {
        const shiftTrack = ties[ties.length - 1][0].track;

        const extent = d3.extent(ties.map(tie => tie[0].start));

        let leftmostShift = 0;
        let rightmostShift = 0;

        let leftmostBias = 0;
        let rightmostBias = 0;

        ties.forEach(tie => {
            const shift = tie[1].start - tie[0].start;

            const rightBias = (tie[0].start - extent[0]) / (extent[1] - extent[0]);
            rightmostShift += shift * rightBias;
            rightmostBias += rightBias;

            const leftBias = 1 - rightBias;
            leftmostShift += shift * leftBias;
            leftmostBias += leftBias;
        });

        leftmostShift /= leftmostBias;
        rightmostShift /= rightmostBias;

        origins = [extent[0], extent[1]];
        targets = [extent[0] + leftmostShift, extent[1] + rightmostShift];

        const rescaleFactor = (targets[1] - targets[0]) / (origins[1] - origins[0]);
        const rebaseShift = targets[0] - origins[0] * rescaleFactor;

        tracks[shiftTrack].subtitles.forEach(subtitle => {
            subtitle.start = subtitle.start * rescaleFactor + rebaseShift;
            subtitle.end = subtitle.end * rescaleFactor + rebaseShift;
        });
    } else if (ties.length === 1) {
        const shiftTrack = ties[0][0].track;
        const shift = (ties[0][1].start - ties[0][0].start);

        tracks[shiftTrack].subtitles.forEach(subtitle => {
            subtitle.start += shift;
            subtitle.end += shift;
        });
    }

    updateSubtitles();
    updateGraphs();

    undoButton.removeAttribute('disabled');
}